const mongoose = require('mongoose')
const Schema = mongoose.Schema

const regexp = /(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/im

const shrinkmeSchema = new Schema({
  _id: Schema.Types.ObjectId,
  originalUrl: {
    type: String,
    required: `Can't shorten an URL without an URL!`,
    validate: {
      validator: function (v) {
        return regexp.test(v)
      },
      message: `Not a valid URL.`
    }
  },
  shrunkUrl: String,
  numVisits: Number,
  creationDate: Date,
  lastVisit: Date
})

exports.ShrunkURL = mongoose.model('URL', shrinkmeSchema)
