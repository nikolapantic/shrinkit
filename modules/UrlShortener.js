const mongoose = require('mongoose')
const ShrunkSchema = require('./ShrunkSchema')
const characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY'
const base62 = require('base-x')(characters)
const characterAmount = 6

const generateShortURLFromObjectId = ObjectId => {
  const buffer = Buffer.from(ObjectId.toHexString(), 'hex')
  const encoded = base62.encode(buffer)
  return encoded.substring(encoded.length - characterAmount)
}

exports.generateShrunkUrl = originalUrl => {
  const objectId = new mongoose.Types.ObjectId()
  return new ShrunkSchema.ShrunkURL({
    _id: objectId,
    originalUrl: originalUrl,
    shrunkUrl: generateShortURLFromObjectId(objectId),
    numVisits: 0,
    creationDate: Date.now(),
    lastVisit: null
  })
}
