import { h, Component } from 'preact'
import Button from '../button'
import TextInput from '../text-input'
import styles from './styles.css'

export default class InputForm extends Component {
  constructor (props) {
    super(props)

    this.state = { value: '' }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleInput = this.handleInput.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    this.props.handleSubmit(Object.assign({}, this.state))
    this.setState({value: ''})
  }

  handleInput (value) {
    this.setState({ value })
  }

  render (props, state) {
    return (
      <form class={styles['input-form']} onSubmit={this.handleSubmit}>
        <TextInput value={state.value} onInput={this.handleInput} placeholder='Insert a proper formatted URL to shrink it' />
        <Button text={props.buttonText} />
      </form>
    )
  }
}
