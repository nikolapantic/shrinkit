import { h } from 'preact'
import styles from './styles.css'

export default () => (
  <h1 class={styles.logo}>ShrinkIt!</h1>
)
