import { h } from 'preact'
import styles from './styles.css'

export default props => {
  return (
    <div class={styles['card-container']}>
      { props.children }
    </div>
  )
}
