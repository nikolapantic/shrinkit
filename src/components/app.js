import { h, Component } from 'preact'
import MainSection from './main-section'
import Footer from './footer'
import InputForm from './input-form'
import CardContainer from './card-container'
import Card from './card'
import styles from './styles.css'

export default class App extends Component {
  constructor () {
    super()
    this.state = {
      savedUrls: [],
      latestShrunk: null
    }
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }

  componentWillMount () {
    const urls = JSON.parse(window.localStorage.getItem('urls')) || []
    const shrunks = urls.map(url => url.shrunkUrl)
    fetch(`/getObjsByShrunk/${JSON.stringify(shrunks)}`)
      .then(result => result.json())
      .then(result => {
        this.setState({
          savedUrls: result
        })
      })
  }

  handleFormSubmit (e) {
    const postOps = {
      method: 'POST',
      body: JSON.stringify({url: e.value}),
      headers: { 'content-type': 'application/json' }
    }
    fetch('/shrinkUrl', postOps)
      .then(result => result.json())
      .then(shrunk => {
        const savedShrunks = this.state.savedUrls.map(url => url.shrunkUrl)
        if (!savedShrunks.includes(shrunk.shrunkUrl)) {
          this.setState({
            savedUrls: [shrunk, ...this.state.savedUrls].slice(0, 10)
          })
          window.localStorage.setItem('urls', JSON.stringify(this.state.savedUrls))
        }
        this.setState({ latestShrunk: shrunk.shrunkUrl })
      })
  }

  render (props, state) {
    return (
      <div class={styles.app}>
        <MainSection latestShrunk={state.latestShrunk}>
          <InputForm handleSubmit={this.handleFormSubmit} buttonText='Shrink!' />
        </MainSection>
        <CardContainer>
          {
            state.savedUrls.map(url => (
              <Card
                originalUrl={url.originalUrl}
                shortUrl={url.shrunkUrl}
                lastVisit={url.lastVisit ? new Date(url.lastVisit).toLocaleString() : 'never'}
                visits={url.numVisits}
              />
            ))
          }
        </CardContainer>
        <Footer text='ShrinkIt - Just another URL shortening service' />
      </div>
    )
  }
}
