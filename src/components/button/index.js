import { h } from 'preact'
import styles from './styles.css'

export default props => (
  <button class={styles.button}>{props.text}</button>
)
