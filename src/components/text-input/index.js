import { h, Component } from 'preact'
import styles from './styles.css'

export default class TextInput extends Component {
  constructor (props) {
    super(props)

    this.handleInput = this.handleInput.bind(this)
  }

  handleInput (e) {
    this.props.onInput(e.target.value)
  }

  render (props) {
    return (
      <input class={styles.input} value={props.value} onInput={this.handleInput} placeholder={props.placeholder} type='url' required />
    )
  }
}
