import { h } from 'preact'
import styles from './styles.css'

export default props => (
  <div class={styles.card}>
    <a title={props.originalUrl} class={styles.link} href={props.shortUrl}>{'/' + props.shortUrl}</a>
    <div>
      <div class={styles.category}>
        <span class={styles.title}>Visits:</span>
        <span class={styles.value}>{props.visits}</span>
      </div>
      <div class={styles.category}>
        <span class={styles.title}>Last visit:</span>
        <span class={styles.value}>{props.lastVisit}</span>
      </div>
    </div>
  </div>
)
