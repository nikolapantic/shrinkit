import { h } from 'preact'
import styles from './styles.css'

export default props => (
  <footer class={styles.footer}>{props.text}</footer>
)
