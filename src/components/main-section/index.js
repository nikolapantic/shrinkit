import { h } from 'preact'
import Logo from '../logo'
import styles from './styles.css'

export default props => (
  <main class={styles.main}>
    <Logo />
    {props.children}
    <p class={props.latestShrunk ? styles.p : styles.hidden}>
     Shrunk'd URL: <a class={styles.link} href={props.latestShrunk}>{window.location.href + props.latestShrunk}</a>
    </p>
  </main>
)
