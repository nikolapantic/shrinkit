# ShrinkIt!

ShrinkIt! is just another URL shortening service. Really.

## Getting Started

Start by cloning this repository, cd into it and install all necessary files like so:
```
$ git clone https://nikolapantic@bitbucket.org/nikolapantic/shrinkit.git
$ cd shrinkit
$ npm install
```

### Prerequisites

This application relies on Node.js and the NPM package manager and these need to be installed (if not already present):
Node.js & NPM - https://nodejs.org/en/

This application is also running a MongoDB backend. No restore of data dump is necessary so running a plain and fresh instance will work.

Download and install it from:
MongoDB - https://www.mongodb.com/

### Installing

Start by building the application:

```
$ npm run build
```

Start the MongoDB instance:
```
$ sudo mongod
```

Start the server:
```
$ node server.js
```

And visit the application:
```
http://localhost:3000/
```

## Running the tests

Unfortunately there are currently no tests :( Working on it!

## Built With

* [Preact](https://preactjs.com/) - UI library
* [Express](https://expressjs.com/) - Web framework
* [MongoDB](https://www.mongodb.com/) - NoSQL database for storing all data
* [Mongoose](http://mongoosejs.com/) - Object modeling for MongoDB
* [Webpack](https://webpack.js.org/) - To bundle it all up

## Authors

* **Nikola Pantic** - *Initial work/prototype* - [nikkop](https://github.com/nikkop)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details