const path = require('path')
const express = require('express')
const mongoose = require('mongoose')
const PORT = 3000
const dbPath = 'mongodb://localhost/shrinkme'
const app = express()
const { generateShrunkUrl } = require('./modules/UrlShortener')
const { ShrunkURL } = require('./modules/ShrunkSchema')

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(express.static(path.join(__dirname, 'build')))

app.get('/:url([0-9a-zA-Z]{6})', async (req, res) => {
  await mongoose.connect(dbPath)
  const result = await ShrunkURL.findOneAndUpdate(
    { shrunkUrl: req.params.url },
    {
      $inc: { numVisits: 1 },
      lastVisit: Date.now()
    }
  )
  result ? res.redirect(result.originalUrl) : res.redirect('/')
})

app.get('/getObjsByShrunk/:shrunks', async (req, res) => {
  const shrunks = JSON.parse(req.params.shrunks)
  await mongoose.connect(dbPath)
  const shrunk = await ShrunkURL.find({
    shrunkUrl: { $in: shrunks }
  }).sort('-lastVisit')
  res.send(shrunk)
})

app.post('/shrinkUrl', async (req, res) => {
  await mongoose.connect(dbPath)
  res.send(
    await ShrunkURL.findOne({ originalUrl: req.body.url }) ||
    await generateShrunkUrl(req.body.url).save()
  )
})

app.use((req, res) => res.status(404).redirect('/'))

app.listen(PORT, () => console.log(`Listening on port: ${PORT}`))

const errHandler = err => {
  if (err) {
    console.error(err)
  }
}
